package com.service;

import com.pojo.Customer;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/insurancePremium")
public class HealthInsurancePremium {

    @POST
    @Path("/getQuote")
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String getQuote(@FormParam("name") String name, @FormParam("gender") String gender,
            @FormParam("age") Integer age, @FormParam("Hypertension") Boolean Hypertension,
            @FormParam("Bloodpressure") Boolean Bloodpressure, @FormParam("Bloodsugar") Boolean Bloodsugar,
            @FormParam("Overweight") Boolean Overweight, @FormParam("Smoking") Boolean Smoking,
            @FormParam("Alcohol") Boolean Alcohol, @FormParam("Dailyexercise") Boolean Dailyexercise,
            @FormParam("Drugs") Boolean Drugs) {
        Customer cust = new Customer();
        cust.setName(name);
        cust.setGender(gender);
        cust.setAge(age);
        cust.setHypertension(Hypertension);
        cust.setBloodpressure(Bloodpressure);
        cust.setBloodsugar(Bloodsugar);
        cust.setOverweight(Overweight);
        cust.setSmoking(Smoking);
        cust.setAlcohol(Alcohol);
        cust.setDailyexercise(Dailyexercise);
        cust.setDrugs(Drugs);

        double initialCost = 5000.00;
        Boolean value = true;
        int age1 = cust.getAge();

        if (age1 > 18 && age1 <= 40) {
            int num = age1 - 18;
            while (num > 4) {
                initialCost = initialCost + initialCost * 10 / 100;
                num = num - 5;
            }

        } else {
            int num = age1 - 18;
            while (num > 4) {
                initialCost = initialCost + initialCost * 20 / 100;
                num = num - 5;
            }
        }

        if (cust.getGender().equals("M")) {
            initialCost = initialCost + initialCost * 2 / 100;
        }
        int currentHealth = 0;
        if (cust.getHypertension() == value) {
            currentHealth++;
        }
        if (cust.getBloodpressure() == value) {
            currentHealth++;
        }
        if (cust.getBloodsugar() == value) {
            currentHealth++;
        }
        if (cust.getOverweight() == value) {
            currentHealth++;
        }
        initialCost = initialCost + initialCost * currentHealth / 100;

        int habits = 0;
        if (cust.getSmoking() == value) {
            habits = habits - 3;
        }
        if (cust.getAlcohol() == value) {
            habits = habits - 3;
        }
        if (cust.getDrugs() == value) {
            habits = habits - 3;
        }
        if (cust.getDailyexercise() == value) {
            habits = habits + 3;
        }

        initialCost = initialCost + initialCost * habits / 100;

        return "{\"status\": \"" + Math.round(initialCost) + "\"}";
    }

}
