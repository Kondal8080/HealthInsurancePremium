/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pojo;

/**
 *
 * @author ebs-sdd32
 */
public class Customer {

    String name;
    String gender;
    Integer age;

    Boolean Hypertension;
    Boolean Bloodpressure;
    Boolean Bloodsugar;
    Boolean Overweight;

    Boolean Smoking;
    Boolean Alcohol;
    Boolean Dailyexercise;
    Boolean Drugs;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getHypertension() {
        return Hypertension;
    }

    public void setHypertension(Boolean Hypertension) {
        this.Hypertension = Hypertension;
    }

    public Boolean getBloodpressure() {
        return Bloodpressure;
    }

    public void setBloodpressure(Boolean Bloodpressure) {
        this.Bloodpressure = Bloodpressure;
    }

    public Boolean getBloodsugar() {
        return Bloodsugar;
    }

    public void setBloodsugar(Boolean Bloodsugar) {
        this.Bloodsugar = Bloodsugar;
    }

    public Boolean getOverweight() {
        return Overweight;
    }

    public void setOverweight(Boolean Overweight) {
        this.Overweight = Overweight;
    }

    public Boolean getSmoking() {
        return Smoking;
    }

    public void setSmoking(Boolean Smoking) {
        this.Smoking = Smoking;
    }

    public Boolean getAlcohol() {
        return Alcohol;
    }

    public void setAlcohol(Boolean Alcohol) {
        this.Alcohol = Alcohol;
    }

    public Boolean getDailyexercise() {
        return Dailyexercise;
    }

    public void setDailyexercise(Boolean Dailyexercise) {
        this.Dailyexercise = Dailyexercise;
    }

    public Boolean getDrugs() {
        return Drugs;
    }

    public void setDrugs(Boolean Drugs) {
        this.Drugs = Drugs;
    }

}
